const template = document.createElement('template')
template.innerHTML = `
<style>
.user-card {
    border: 1px solid #999;
    border-radius: 5px;
    display: inline-block;
    vertical-align: top;
    padding: 1rem;
}

.user-name {
    font-size: 14px;
    text-align: left;
    padding-top: 5px;
}

.user-actions {
text-align: right;
}

</style>
<div class="user-card">
    <div class="user-img">
        <img />
    </div>
    <div class="user-name"></div>
    <div class="user-info">
        <ul>
            <li><slot name="email"></slot></li>
            <li><slot name="phone"></slot></li>
        </ul>
    </div>
    <div class="user-actions"><button id="toggle-info">Hide Info</button></div>
</div>
`

class UserCard extends HTMLElement {
    constructor() {
        super();
        this.showInfo = true;
        this.attachShadow({ mode: 'open' });
        this.shadowRoot.appendChild(template.content.cloneNode(true));
        this.shadowRoot.querySelector('.user-name').innerHTML = this.getAttribute('name');
        this.shadowRoot.querySelector('.user-img img',).src = this.getAttribute('avatar');
    }

    toggleInfo () {
        this.showInfo = !this.showInfo;

        const info = this.shadowRoot.querySelector(".user-info");
        const toggleBtn = this.shadowRoot.querySelector("#toggle-info");

        if (this.showInfo) {
            info.style.display = 'block';
            toggleBtn.innerText = 'Hide Info'
        } else {
            info.style.display = 'none';
            toggleBtn.innerText = 'Show Info'
        }
    }

    connectedCallback () {
        this.shadowRoot.querySelector('#toggle-info').addEventListener('click', () => this.toggleInfo());
        this.toggleInfo();
    }

    disconnectedCallback () {
        this.shadowRoot.querySelector('#toggle-info').removeEventListener();
    }
}

window.customElements.define('user-card', UserCard);